import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { Observable } from 'rxjs';
import { User } from './models/user';

@Component({
  selector: 'app-root',
  template: `
    <div fxLayout="column" fxLayoutAlign="space-between stretch" fxLayoutGap="20px">
      <h1>List of users:</h1>
      <app-search [allUsers]="users$ | async" (filteredUsersItemEvent)="filterUsers($event)"></app-search>
      <mat-accordion multi>
        <app-user *ngFor="let user of users$ | async" [user]="user"></app-user>
      </mat-accordion>
    </div>
  `
})
export class AppComponent implements OnInit {
  public users$: Observable<User[]>;

  constructor(private userService: UserService) {
  }

  public ngOnInit(): void {
    this.users$ = this.userService.getAll();
  }

  public filterUsers(users: string[]): void {
    this.userService.filter(users);
  }
}
