import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-feature',
  template: `
    <div fxLayout="row" fxLayoutAlign="space-between center">
      <span>{{label}}</span>
      <mat-icon *ngIf="active"  class="active">done</mat-icon>
      <mat-icon *ngIf="!active"  class="inactive">close</mat-icon>
    </div>
  `,
  styles: [
    '.inactive {color: gray}',
    '.active {color: #1ae876}'
  ]
})
export class FeatureComponent {
  @Input()
  public label: string;
  @Input()
  public active: boolean;
}
