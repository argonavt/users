import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MatChipInputEvent } from '@angular/material/chips';
import { User } from '../../models/user';

@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html',
  styles: [
    '.chip-list { width: 100%; }'
  ]
})
export class SearchComponent  {
  public separatorKeysCodes: number[] = [ENTER, COMMA];
  public userCtrl = new FormControl();
  public filteredUsers: Observable<User[]>;
  public users: string[] = [];

  @Input()
  public allUsers: User[];

  @Output()
  public filteredUsersItemEvent = new EventEmitter<string[]>();

  @ViewChild('userInput')
  public userInput: ElementRef<HTMLInputElement>;

  @ViewChild('auto')
  public matAutocomplete: MatAutocomplete;

  constructor() {
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(null),
      map((user: string | null) => user ? this._filter(user) : this.allUsers.slice()));
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.users.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.userCtrl.setValue(null);
    this.filteredUsersItemEvent.emit(this.users);
  }

  remove(user: string): void {
    const index = this.users.indexOf(user);

    if (index >= 0) {
      this.users.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.users.push(event.option.viewValue);
    this.userInput.nativeElement.value = '';
    this.userCtrl.setValue(null);
  }

  private _filter(value: string): User[] {
    const filterValue = value.toLowerCase();

    return this.allUsers.filter(user => this._findUser(filterValue, user.name));
  }

  private _findUser(filterValue: string, userName: string): boolean {
    const [firstName, lastNam] = userName.split(' ');
    if (firstName.toLowerCase().indexOf(filterValue) === 0){
      return true;
    }
    return lastNam.toLowerCase().indexOf(filterValue) === 0;
  }
}
