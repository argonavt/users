import { Component, Input } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html',
  styles: [
    '.tab-wrapper {margin-top: 20px;}'
  ]
})
export class UserComponent {
  @Input()
  public user: User;
}
