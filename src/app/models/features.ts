import { Deserializable } from './deserializable';

export class Features implements Deserializable {
  public conferenceCalling: boolean;
  public callWaiting: boolean;
  public voicemail: boolean;

  public deserialize(input: any): this {
    Object.assign(this, input);
    return this;
  }
}
