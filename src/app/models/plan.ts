import { Features } from './features';
import { Deserializable } from './deserializable';

export class Plan implements Deserializable {
  public type: string;
  public status: string;
  public description: string;
  public features: Features;

  public deserialize(input: any): this {
    Object.assign(this, input);
    this.features = new Features().deserialize(input.features);
    return this;
  }

  public isActive(): boolean {
    return 'active' === this.status;
  }
}
