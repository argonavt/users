import { Plan } from './plan';
import { Deserializable } from './deserializable';

export class User implements Deserializable {
  public id: string;
  public name: string;
  public email: string;
  public age: number;
  public plan: Plan;

  public deserialize(input: any): this {
    Object.assign(this, input);
    this.plan = new Plan().deserialize(input.plan);
    return this;
  }
}
