import { Injectable } from '@angular/core';
import { User } from '../models/user';
import mockUsers from '../../mock-users.json';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private subject = new BehaviorSubject<any>(mockUsers.users);

  public getAll(): Observable<User[]> {
    return this.subject.asObservable().pipe(
      map(data =>  data.map(user => new User().deserialize(user)))
    );
  }

  public filter(users: string[]): void {
    if (users.length){
      this.subject.next(mockUsers.users.filter(user => users.includes(user.name)));
    }else{
      this.subject.next(mockUsers.users);
    }
  }
}
